extends KinematicBody
onready var ball = $RigidBody
export var gravity = -50.0
export var wheel_base = 0.6  
export var steering_limit = 10.0  
export var engine_power = 6.0
export var braking = -9.0
export var friction = -2.0
export var drag = -2.0
export var max_speed_reverse = 3.0
export var speed = 5
export var slip_speed = 9.0
export var traction_slow = 0.75
export var traction_fast = 0.02
var body_tilt = 35
var drifting = false
var acceleration = Vector3.ZERO 
var velocity = Vector3.ZERO  
var steer_angle = 0.0 
var start = false
var oynat = false
func _physics_process(delta):
	get_input()
	apply_friction(delta)
	calculate_steering(delta)
	velocity.y += gravity * delta
	velocity += acceleration * delta 
	velocity = move_and_slide_with_snap(velocity, Vector3.DOWN*2, Vector3.UP, true)
	if Input.is_action_just_released("start"):
		start = true
	if Input.is_action_pressed("fren"):
		acceleration = transform.basis.z * engine_power * speed 
		velocity += acceleration * delta
	

	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider is KinematicBody:
			get_tree().change_scene("res://kaybettiniz.tscn")
func apply_friction(delta):
	if velocity.length() < 1.8 and acceleration.length() == 0:
		velocity.x = 0
		velocity.z = 0
	var friction_force = velocity * friction * delta
	var drag_force = velocity * velocity.length() * drag * delta
	acceleration += drag_force + friction_force 
func calculate_steering(delta):
	var rear_wheel = transform.origin + transform.basis.z * wheel_base / 2.0
	var front_wheel = transform.origin - transform.basis.z * wheel_base / 2.0
	rear_wheel += velocity * delta
	front_wheel += velocity.rotated(transform.basis.y, steer_angle) * delta
	var new_heading = rear_wheel.direction_to(front_wheel)
	if not drifting and velocity.length() > slip_speed:
		drifting = true
	if drifting and velocity.length() < slip_speed and steer_angle == 0:
		drifting = false
	var traction = traction_fast if drifting else traction_slow

	var d = new_heading.dot(velocity.normalized())
	if d > 0:
		velocity = lerp(velocity, new_heading * velocity.length(), traction)
	if d < 0:
		velocity = -new_heading * min(velocity.length(), max_speed_reverse)
	look_at(transform.origin + new_heading, transform.basis.y)
	var ada = -steer_angle * ball.linear_velocity.length() / body_tilt
	$sedan/body.rotation.z = lerp($sedan/body.rotation.z, ada, 4 * delta)
func get_input():
	acceleration = Vector3.ZERO
	if start == true:
		if Input.is_action_pressed("accelerate"):
			acceleration = -transform.basis.z * engine_power * speed
		if Input.is_action_pressed("brake"):
			acceleration = transform.basis.z * engine_power * speed
		
		var turn = Input.get_action_strength("steer_left")
		turn -= Input.get_action_strength("steer_right")
		steer_angle = turn * deg2rad(steering_limit) * 0.2
		$sedan/wheel_frontRight.rotation.y = steer_angle*2
		$sedan/wheel_frontLeft.rotation.y = steer_angle*2
		if Input.is_action_just_released("çıkış"):
			start = false











