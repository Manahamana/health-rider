extends KinematicBody
func get_input():
	acceleration = Vector3.ZERO
	if start == true:
		if Input.is_action_pressed("accelerate"):
			acceleration = -transform.basis.z * engine_power * speed
		if Input.is_action_pressed("brake"):
			acceleration = transform.basis.z * engine_power * speed
		
		var turn = Input.get_action_strength("steer_left")
		turn -= Input.get_action_strength("steer_right")
		steer_angle = turn * deg2rad(steering_limit) * 0.2
		$sedan/wheel_frontRight.rotation.y = steer_angle*2
		$sedan/wheel_frontLeft.rotation.y = steer_angle*2
		if Input.is_action_just_released("çıkış"):
			start = false


