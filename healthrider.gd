extends KinematicBody
onready var ball = $RigidBody
export var gravity = -50.0
export var wheel_base = 0.6  
export var steering_limit = 10.0  
export var engine_power = 6.0
export var braking = -9.0
export var friction = -2.0
export var drag = -2.0
export var max_speed_reverse = 3.0
export var speed = 20

export var rotate_speed = 4
export var rotate_speed2 = -4
export var slip_speed = 9.0
export var traction_slow = 0.75
export var traction_fast = 0.02
var body_tilt = 35
var drifting = false
var acceleration = Vector3.ZERO 
var velocity = Vector3.ZERO  
var steer_angle = 0.0 
var start = false
var oynat = false
var r = 0
func _physics_process(delta):
	velocity = -transform.basis.z * speed 
	velocity = move_and_slide(velocity, Vector3.UP)
	calculate_steering(delta)
	if abs(steer_angle) > 10:
		speed = 20
func calculate_steering(delta):
	if $RayCast.is_colliding() or $RayCast2.is_colliding() or $RayCast3.is_colliding():
		var rear_wheel = transform.origin + transform.basis.z * wheel_base / 2.0
		var front_wheel = transform.origin - transform.basis.z * wheel_base / 2.0
		rear_wheel += velocity * delta
		front_wheel += velocity.rotated(transform.basis.y, steer_angle) * delta
		var new_heading = rear_wheel.direction_to(front_wheel)

		var d = new_heading.dot(velocity.normalized())
		if d > 0:
				velocity = new_heading * velocity.length()
		if d < 0:
			velocity = -new_heading * min(velocity.length(), max_speed_reverse)
		look_at(transform.origin + new_heading, transform.basis.y)
		if $RayCast.is_colliding():
			steer_angle = rotate_speed * delta
		if $RayCast2.is_colliding():
			steer_angle = rotate_speed2 * delta 
		
		if $RayCast3.is_colliding():
			steer_angle = rotate_speed * delta
			







